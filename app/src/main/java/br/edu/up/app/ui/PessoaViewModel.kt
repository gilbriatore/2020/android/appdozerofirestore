package br.edu.up.app.ui

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import br.edu.up.app.model.Pessoa
import br.edu.up.app.repository.PessoaRepository

class PessoaViewModel(app: Application) : AndroidViewModel(app) {

    var pessoa = MutableLiveData<Pessoa>()
    var repository = PessoaRepository()
    var listaDePessoas = repository.listaDePessoas

}